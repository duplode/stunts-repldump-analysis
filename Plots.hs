module Plots
    ( writePlot
    , module Util
    ) where

import Data.Colour.Names
import Data.Colour
import Control.Lens
import Data.Default.Class
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Backend.Cairo

import GameState
import Util

-- Meant for interactive exploration.

plotFields fx fys gss = toRenderable layout
  where

    prj (cl, (n, fy)) = plot_lines_style . line_color .~ cl
                      $ plot_lines_values .~ [map (mkPoint fx fy) gss]
                      $ plot_lines_title .~ show n
                      $ def


    mkPoint fx fy gs =
        (realToFrac . fx $ gs, realToFrac . fy $ gs) :: (Double, Double)

    colours = cycle $ map opaque
        [ blue, red, green, yellow, cyan, magenta, black, orange, grey ]

    projections = map prj . zip colours . zip [1..] $ fys

    layout = layout1_title .~"Game state exploration"
           $ layout1_left_axis . laxis_override .~ axisGridHide
           $ layout1_right_axis . laxis_override .~ axisGridHide
           $ layout1_bottom_axis . laxis_override .~ axisGridHide
           $ layout1_plots .~ map (Left . toPlot) projections
           $ layout1_grid_last .~ False
           $ def

writePlot fx fys gss = renderableToPNGFile (plotFields fx fys gss) 800 600 "test.png"

