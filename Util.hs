module Util where

fst3, snd3, thd3 :: (a, a, a) -> a
fst3 (x, _, _) = x
snd3 (_, y, _) = y
thd3 (_, _, z) = z

